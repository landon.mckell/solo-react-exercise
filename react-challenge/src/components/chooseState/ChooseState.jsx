import { STATES } from "../../utils/states";
const ChooseState = (props) => {
  return (
    <div className="ChooseState">
      <h6>Choose a State</h6>
      <select
        className="form-select"
        onChange={(e) => props.callback(e.target.value)}
        required
      >
        <option selected disabled value="">
          Choose a State
        </option>
        {STATES.map((state) => (
          <option key={state} value={state.substring(0, 2)}>
            {state}
          </option>
        ))}
      </select>
    </div>
  );
};

export default ChooseState;
