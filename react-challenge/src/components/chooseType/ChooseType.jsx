import React from "react";

const ChooseType = (props) => {
  return (
    <div className="ChooseType">
      <h6>Representatives or Senators</h6>
      <select
        className="form-select"
        onChange={(e) => props.callback(e.target.value)}
        required
      >
        <option selected disabled value="">Choose Rep or Sen</option>
        <option value="reps">Representative</option>
        <option value="sens">Senator</option>
      </select>
    </div>
  );
};

export default ChooseType;
