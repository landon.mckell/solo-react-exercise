import React from "react";

const InformationList = (props) => {
  if (!props.memberInfo) return;
  console.log(props.memberInfo);
  return (
    <ul className="list-group">
      <li className="list-group-item active" aria-current="true">
        INFO
      </li>
      <li className="list-group-item d-flex justify-content-between align-items-start">
        <div className="ms-2 me-auto">
          <div className="fw-bold">Name</div>
          {props.memberInfo.name}
        </div>
      </li>
      <li className="list-group-item d-flex justify-content-between align-items-start">
        <div className="ms-2 me-auto">
          <div className="fw-bold">Party</div>
          {props.memberInfo.party}
        </div>
      </li>
      <li className="list-group-item d-flex justify-content-between align-items-start">
        <div className="ms-2 me-auto">
          <div className="fw-bold">District</div>
          {props.memberInfo.district}
        </div>
      </li>
      <li className="list-group-item d-flex justify-content-between align-items-start">
        <div className="ms-2 me-auto">
          <div className="fw-bold">Phone</div>
          {props.memberInfo.phone}
        </div>
      </li>
      <li className="list-group-item d-flex justify-content-between align-items-start">
        <div className="ms-2 me-auto">
          <div className="fw-bold">Office</div>
          {props.memberInfo.office}
        </div>
      </li>
      <li className="list-group-item d-flex justify-content-between align-items-start">
        <div className="ms-2 me-auto">
          <div className="fw-bold">Link</div>
          <a
            href={props.memberInfo.link}
            target="_blank"
            rel="noopener noreferrer"
          >
            {props.memberInfo.link}
          </a>
        </div>
      </li>
    </ul>
  );
};

export default InformationList;
