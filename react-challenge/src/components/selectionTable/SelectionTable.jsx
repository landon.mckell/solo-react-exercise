const SelectionTable = (props) => {
  function renderData() {
    if (!props.data) return;
    return props.data.map((item) => (
      <tr
        key={item.name}
        onClick={() => props.rowCallback(item)}
        style={{ cursor: "pointer" }}
      >
        <td>{item.name}</td>
        <td>{item.party}</td>
      </tr>
    ));
  }
  return (
    <table className="table table-hover">
      <thead>
        <tr>
          <th>{props.isRepresentative ? "Representative" : "Senator"} Name</th>
          <th>Party</th>
        </tr>
      </thead>
      <tbody>
        {props.data ? (
          renderData()
        ) : (
          <tr>
            <td>There's nothing here...</td>
            <td></td>
          </tr>
        )}
      </tbody>
    </table>
  );
};

export default SelectionTable;
