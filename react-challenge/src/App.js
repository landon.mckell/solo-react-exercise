import { useState } from "react";
import axios from "axios";
import ChooseState from "./components/chooseState/ChooseState";
import ChooseType from "./components/chooseType/ChooseType";
import SelectionTable from "./components/selectionTable/SelectionTable";
import InformationList from "./components/informationList/InformationList";
function App() {
  const [formData, setFormData] = useState({
    type: "",
    state: "",
  });
  const [memberData, setMemberData] = useState();
  const [memberDetails, setMemberDetails] = useState();

  const getData = async (e) => {
    e.preventDefault();
    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    };
    const { data } = await axios.get(
      `/${formData.type === "reps" ? "representatives" : "senators"}/${
        formData.state
      }`,
      config
    );
    setMemberData(data.results);
    setMemberDetails();
  };
  return (
    <div className="container py-2">
      <form className="needs-validation" noValidate onSubmit={getData}>
        <div className="row mb-3 ">
          <div className="col-12 col-sm-3 mb-2 mb-sm-0">
            <ChooseType
              callback={(selection) => {
                setFormData((prevState) => {
                  return {
                    ...prevState,
                    type: selection,
                  };
                });
              }}
            />
          </div>
          <div className="col-12 col-sm-3">
            <ChooseState
              callback={(val) => {
                setFormData((prevState) => {
                  return {
                    ...prevState,
                    state: val,
                  };
                });
              }}
            />
          </div>
          <div className="col-12 col-sm-2 mt-3 mt-sm-auto">
            <button className="btn btn-success" type="submit">
              Submit
            </button>
          </div>
        </div>
      </form>

      <div className="row">
        <div className="col-12 col-sm-7 order-1 order-sm-0">
          <SelectionTable
            isRepresentative={formData.type === "reps"}
            data={memberData}
            rowCallback={(val) => {
              setMemberDetails(val);
            }}
          />
        </div>
        <div className="col-12 col-sm-5 order-0 order-sm-1">
          <InformationList memberInfo={memberDetails} />
        </div>
      </div>
    </div>
  );
}

export default App;
